const localStorageConfig = {
  favCoins: 'GYfXORPwLt',
};

const { favCoins } = localStorageConfig;

export const setFavCoin = (coin) => {
  try {
    let favCoinsList = getFavCoin();
    favCoinsList = { ...favCoinsList, ...coin };
    localStorage.setItem(favCoins, JSON.stringify(favCoinsList));
  } catch (err) {
    console.log(err);
  }
};

export const getFavCoin = () => {
  try {
    return JSON.parse(localStorage.getItem(favCoins)) || {};
  } catch (err) {
    console.log(err);
  }
};


