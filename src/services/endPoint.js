export const API_VERSION = '';

/* User */
export const GET_COINS = `${API_VERSION}/jugApi/coinParams.json`;

/* Search */
export const GET_TICKER_WITH_VOLUME = `${API_VERSION}/order/getTickerWithVolume/`;

