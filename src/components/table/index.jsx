import React from 'react';
import MaterialTable from 'material-table';
import { useHistory } from 'react-router-dom';


export default function TableComponent({ columns = [], rowData = [] }) {
    const history = useHistory();
    return (
        <MaterialTable
            title=""
            columns={columns}
            data={rowData}
            onRowClick={((evt, selectedRow) => history.push({ pathname: `/coin/${selectedRow.coinName}`, state: selectedRow }))}
        />
    )
}


