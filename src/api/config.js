import API from ".";
import { GET_COINS, GET_TICKER_WITH_VOLUME } from "../services/endPoint";


export const getCoinsApi = (config = {}) => API.get(GET_COINS, { ...config });
export const getTickerWithVolumeApi = (config = {}) => API.get(GET_TICKER_WITH_VOLUME, { ...config });



