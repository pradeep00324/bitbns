import { makeStyles } from '@material-ui/core/styles';
import React, { useEffect, useState } from 'react';
import TableComponent from '../../components/table';
import {
    Link
} from 'react-router-dom';
import { getCoinsApi, getTickerWithVolumeApi } from "../../api/config";
import { getFavCoin, setFavCoin } from '../../services/localStorageServices';

import StarIcon from '@material-ui/icons/Star';
const useStyles = makeStyles((theme) => ({
    paper: {
        margin: '24px',
        padding: '24px',
    },
    tableContainer: {
        margin: '24px',
    }
}));


function Dashboard() {
    const classes = useStyles();
    const [rowData, setData] = useState([]);
    const favCoinsList = getFavCoin();
    const [favCoinsInState, setFavCoinInState] = useState(favCoinsList)


    const getVolume = (tempRowData) => {
        getTickerWithVolumeApi()
            .then((res) => {
                let obj = {}
                const tempData = tempRowData.map((d) => {

                    obj = res.data[d?.coinType?.toUpperCase()];
                    return {
                        ...d,
                        ...obj
                    }
                })
                setData(tempData);
            })
            .catch((err) => {
                console.log(err)
            })
    }

    const getCoins = async () => {
        const getCoinsResponse = await getCoinsApi();
        const { data } = getCoinsResponse.data[0];
        const tempData = Object.keys(data[0]).map((key) => {
            return data[0][key]
        })
        setData(tempData);
        getVolume(tempData);
    }


    const init = () => {
        getCoins()
    }

    const handleFavIconClick = (e, rowData) => {
        e.stopPropagation();
        setFavCoinInState({ ...favCoinsList, [rowData.coinId]: favCoinsList[rowData.coinId] ? false : true })
        setFavCoin({ [rowData.coinId]: favCoinsList[rowData.coinId] ? false : true })
    }

    const columns = [
        {
            title: '', field: '', render: rowData => <React.Fragment>
                <StarIcon style={favCoinsList[rowData.coinId] ? { color: 'yellow' } : {}} onClick={(e) => handleFavIconClick(e, rowData)} />
            </React.Fragment>
        },
        { title: 'Avatar', field: 'coinIcon', render: rowData => <Link to={{ pathname: `/coin/${rowData.coinName}`, state: rowData }}><img src={rowData.coinIcon} alt={rowData.coinName} style={{ width: 40, borderRadius: '50%' }} /></Link> },
        { title: 'Coin Name', field: 'coinName' },
        { title: 'MinDeposit', field: 'minDeposit' },
        { title: 'Withdrawal Fee', field: 'withdrawalFee' },
        { title: '24H High', field: 'highest_buy_bid', render: rowData => <div> $ {rowData?.highest_buy_bid || 0}</div> },
        { title: '24H Low', field: 'lowest_sell_bid', render: rowData => <div>$ {rowData?.lowest_sell_bid || 0}</div> },
        { title: 'Volume', field: 'lowest_sell_bid', render: rowData => <div>$ {rowData?.volume?.max || 0}</div> },
    ]

    useEffect(init, []);

    return (
        <div className={classes.tableContainer}>
            <TableComponent columns={columns} rowData={rowData} />
        </div>
    );
}
export default Dashboard;