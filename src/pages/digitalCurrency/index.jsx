import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import ApexChart from '../../components/chart';
import Grid from '@material-ui/core/Grid';
import { useHistory } from 'react-router-dom'

const useStyles = makeStyles((theme) => ({
    paper: {
        margin: '24px',
        padding: '24px',
    },
    tableContainer: {
        margin: '24px',
    }
}));


function DigitalCurrency() {
    const { location: { state } } = useHistory()
    const classes = useStyles();
    return (
        <div >
            <Grid container className={classes.root} spacing={2}>
                <Grid item xs={12}>
                    <Paper className={classes.paper}>
                        <Grid container alignItems='flex-start'>
                            <Grid item xs={3}>
                                <a href={state.hashLink} target='_blank'> <img src={state.coinIcon} width='100px' height='100px' alt='coinIcon' /></a>
                                <Typography variant="h5" component="h5">
                                    {
                                        state?.coinName
                                    }
                                </Typography>
                            </Grid>

                        </Grid>
                    </Paper>
                </Grid>
                <Grid item xs={6}>
                    <Paper className={classes.paper}><ApexChart data={[state.lowest_sell_bid, state.highest_buy_bid]} /></Paper>
                </Grid>
            </Grid>

        </div>
    );
}
export default DigitalCurrency;